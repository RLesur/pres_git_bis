# pres_git_bis

Présentation de GIT pour le BIS  

Ce repo est un projet `RStudio` : pour l'ouvrir, il faut aller dans `RStudio`, puis sélectionner `File`, `New Project`, `Version Control`, `Git` et coller l'url du repo.  
Ensuite, il faut ouvrir le fichier `git_by_Ruser.Rmd` et cliquer sur `Knit`.
